**Steps for setting the development environment**

Download the entire project as a zip

1. Unzip to <your-path>
2. run the following command for each dependency in the lib folder
mvn install:install-file \
  -DgroupId=com.nttdata \
  -DartifactId=arc-core \
  -Dpackaging=jar \
  -Dversion=1.0-SNAPSHOT \
  -Dfile=<PATH-OF-THE-DEPENDENCY> \
  -DgeneratePom=true
3. Import the maven project in your favorite editor
4. Make changes and build the project(Will explain the architecture)
5. cd to lib and run 
mvn spring-boot:run -Dspring-boot.run.jvmArguments="-Darc.mode=analyze"
