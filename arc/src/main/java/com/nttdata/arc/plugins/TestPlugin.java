package com.nttdata.arc.plugins;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.ResourceBundle;

import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.expr.MethodCallExpr;
import com.github.javaparser.ast.expr.VariableDeclarationExpr;
import com.google.inject.Inject;
import com.nttdata.ar4c.core.ASTUtils;
import com.nttdata.ar4c.core.ArcException;
import com.nttdata.ar4c.core.BaseInspectPlugin;
import com.nttdata.ar4c.core.DuplicateInspectionModulesException;
import com.nttdata.ar4c.core.InspectionResolverMediator;
import com.nttdata.ar4c.core.IssueLocation;


/**
 * Hello world!
 *
 */
public class TestPlugin extends BaseInspectPlugin
{

	@Inject
	protected TestPlugin(String issueId, String pluginName, InspectionResolverMediator mediator)
			throws DuplicateInspectionModulesException {
		super("ARC0004", "TestPlugin", mediator);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected ResourceBundle getResourceBundle() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Collection<IssueLocation> getIssueLocations() throws ArcException {
		System.out.println("Hello world!");

        List<CompilationUnit> compilationUnits = super.getConfig().getCompilationUnits();
        
        ASTUtils.getNodes(compilationUnits, ClassOrInterfaceDeclaration.class)
        		.stream()
        		.filter(c -> !c.isInterface()) 
        		.sorted(Comparator.comparingInt(
        				o -> -1 * o.getMethods().size())) 
        		.limit(3)       
        		.forEach(
        				c -> System.out.println(c.getNameAsString() + ": " +c.getMethods().size() + " methods"));


        
        ASTUtils.getNodes(compilationUnits, VariableDeclarationExpr.class)
        		.stream()
        		.forEach(v -> v.walk(c -> System.out.println(c)));
		return null;
	}
	
}
