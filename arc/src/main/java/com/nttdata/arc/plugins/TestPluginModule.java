package com.nttdata.arc.plugins;

import com.nttdata.ar4c.core.ArcModule;

public class TestPluginModule extends ArcModule{

	@Override
	protected void configureActions() {
		bindInspectModule().to(TestPlugin.class);
	}

}
